﻿var input_txt = document.getElementById("input_txt");
var a_name = document.getElementById("a_name");
var a_type = document.getElementById("a_type");
var a_author = document.getElementById("a_author");
var a_publisher = document.getElementById("a_publisher");
var a_isbn = document.getElementById("a_isbn");
var btn = document.getElementById("btn");
a_name.onclick = function () {

    input_txt.setAttribute("placeholder", "请输入" + a_name.innerText);
    //sendData("书名");

    //$.ajax({
    //    url: "/Home/SearchProduct",
    //    data: {
    //        "chooseType":"书名"
    //    },
    //    type: "post",
    //    dataType:"text",
    //    success: function (data) {
    //        //alert(data);
    //    }
    //});
    sendData("书名");
}
a_type.onclick = function () {

    input_txt.setAttribute("placeholder", "请输入" + a_type.innerText);
    //btn.setAttribute("asp-route-chooseType", "类型");
    sendData("类型");
}
a_author.onclick = function () {

    input_txt.setAttribute("placeholder", "请输入" + a_author.innerText);
    //btn.setAttribute("asp-route-chooseType", "作者");
    sendData("作者");
}
a_publisher.onclick = function () {

    input_txt.setAttribute("placeholder", "请输入" + a_publisher.innerText);
    //btn.setAttribute("asp-route-chooseType", "出版社");
    sendData("出版社");
}
a_isbn.onclick = function () {

    input_txt.setAttribute("placeholder", "请输入" + a_isbn.innerText);
    //btn.setAttribute("asp-route-chooseType", "ISBN");
    sendData("ISBN");
}

function sendData(dataType) {
    $.ajax({
        url: "/Home/SearchProduct",
        data: {
            "chooseType": dataType
        },
        type: "post",
        dataType: "text",
        success: function (data) {
            //alert(data);
        }
    });
}