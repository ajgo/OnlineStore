﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Web.Models.Contact;

namespace OnlineStore.Web.Controllers
{
    [Authorize]
    public class ContactController : Controller
    {
        /// <summary>
        /// 用户联系地址主页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 用户修改联系方式
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 用户提交有修改后的联系方式
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Edit(EditViewModel model)
        {
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// 用户添加联系方式
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// 提交用户添加的联系方式
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateViewModel model)
        {
            return RedirectToAction(nameof(Index));
        }
        
        
    }
}