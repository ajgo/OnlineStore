﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OnlineStore.Models;
using OnlineStore.Services;
using OnlineStore.Web.Models;
using OnlineStore.Web.Models.Home;

namespace OnlineStore.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProductService _productService;
        private readonly IAccountService _accountService;

        public HomeController(ILogger<HomeController> logger, IProductService productService)
        {
            _logger = logger;
            _productService = productService;
        }

        public IActionResult Index()
        {
            var indexVm = new IndexViewModel
            {
                CategoryBook = _productService.GetProductByType()
            };

            return View(indexVm);
        }
        
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpPost]        
        public IActionResult SearchProduct(string inputContext,string chooseType)
        {
            var model = new IndexViewModel();
            //chooseType = Request.Form["chooseType"];
            string choose = chooseType;
            if (choose == "书名")
            {
                model = new IndexViewModel
                {
                    
                    CategoryBook = _productService.FindProductByName(inputContext)
                };
                System.Console.WriteLine(model);
            }
            else if (choose == "类型")
            {
                model = new IndexViewModel
                {
                    CategoryBook = _productService.FindProductByName(inputContext)
                };
                System.Console.WriteLine(model);
            }
            else if (choose == "作者")
            {
                model = new IndexViewModel
                {
                    CategoryBook = _productService.FindProductByName(inputContext)
                };
            }
            else if (choose == "出版社")
            {
                model = new IndexViewModel
                {
                    CategoryBook = _productService.FindProductByName(inputContext)
                };
            }
            else if (choose == "ISBN")
            {
                model = new IndexViewModel
                {
                    CategoryBook = _productService.FindProductByName(inputContext)
                };
            }
            else
            {
                model = new IndexViewModel
                {
                    CategoryBook = _productService.GetProductByType()
                };
            }
            return RedirectToAction("Index", "Home", model);
        }
    }
}