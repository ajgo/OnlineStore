﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Models;
using OnlineStore.Services;
using OnlineStore.Web.Models.Orders;
using OnlineStore.Web.Models.ProductDetails;
using OnlineStore.Web.Models.User;

namespace OnlineStore.Web.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {

        private readonly IOrderService _orderService;
        private readonly IAccountService _accountService;
        private readonly ICartService _cartService;
        private readonly IProductService _productService;



        public OrdersController(IOrderService orderService, 
            ICartService cartService,
            IProductService productService,
            IAccountService accountService)
        {
            _orderService = orderService;
            _cartService = cartService;
            _productService = productService;
            _accountService = accountService;
        }


        /// <summary>
        /// 展示当前用户的下单后的订单
        /// 加载订单数据，但是不加载订单详情
        /// </summary>
        /// <returns></returns>
        /// 
        public IActionResult Index()
        {
            IndexOrderViewModel indexOrderViewModel = new IndexOrderViewModel
            {
                wait = _orderService.FindOrderWithState(OrderState.WaitToDeal),
                Shipped = _orderService.FindOrderWithState(OrderState.Shipped),
                completed = _orderService.FindOrderWithState(OrderState.Completed)
            };
            return View(indexOrderViewModel);
        }




        [HttpPost]
        /// <summary>
        /// 展示当前用户的所有订单内容
        /// 加载订单数据，但是不加载订单详情
        /// </summary>
        /// <returns></returns>
        public IActionResult ShowAllInfo()
        {
            var OderVm = new ShowOrdersViewModel
            {
                AllOrdersInfo = _orderService.FindAllOrders()
            };
            return View(OderVm);
        }




        /// <summary>
        /// 用户选择编辑订单
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit()
        {
            var editVm = new Models.Contact.EditViewModel();
            return View(editVm);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Models.Contact.EditViewModel model)
        {
            return RedirectToAction(nameof(Index));
        }


        /// <summary>
        /// 用户选择删除订单(退订单)
        /// </summary>
        /// <returns></returns>
        //public IActionResult Delete()
        //{
        //    var deleteVm = new DeleteViewModel();
        //    return View(deleteVm);
        //}

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteViewModel model)
        {
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// 提交评论到数据库
        /// </summary>
        /// <returns></returns>
        ///        [Authorize]
        public IActionResult SendComment(int ProductId)
        {
            CommentModelView commentModelView = new CommentModelView
            {
                productId = ProductId
            };
            return View(commentModelView);
        }

        [HttpPost]
        public async Task<IActionResult> SendComment(CommentModelView model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);//未添加model
            }
            var SComment = new ProductComment
            {
                Id =model.productId,
                Description = model.commentString,
                Score = model.Score
            };
            await _productService.AddComment(model.productId,Convert.ToInt32( User.Claims.FirstOrDefault(u=>u.Type=="Id").Value), SComment);


            return RedirectToAction(nameof(Index), "Home");
        }

        /// <summary>
        /// 详情页面加载订单详情
        /// </summary>
        /// <param name="uid">对于的订单号</param>
        /// <returns></returns>
        public IActionResult Details(int uid)
        {
            return View();
        }

        public IActionResult AddOrder()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Id");
            AddOrderModel addOrder = new AddOrderModel();
            addOrder.contactInfos = _accountService.GetUserContactInfo(Convert.ToInt32(claim?.Value)).ToList();
            addOrder.productInCart = _cartService.GetProductInCart(Convert.ToInt32(claim?.Value)).ToList();
            return View(addOrder);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder(int contractId)
        {
            var claim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Id");
           var productInCart = _cartService.GetProductInCart(Convert.ToInt32(claim?.Value)).ToList();
            Dictionary<Product, int> products = new Dictionary<Product, int>();
            foreach(var item in productInCart)
            {
                products.Add(item.Product, item.Quantity);
            }
           await _orderService.SubmitOrder(Convert.ToInt32(claim?.Value), contractId,products);
            return RedirectToAction(nameof(Index));
        }
    }
}