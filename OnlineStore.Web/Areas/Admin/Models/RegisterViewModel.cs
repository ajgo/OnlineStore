﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Areas.Admin.Models
{
    public class RegisterViewModel
    {
        [Required]
        [StringLength(20,MinimumLength =3, ErrorMessage = "用户名的长度范围是3-20")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20,MinimumLength =6, ErrorMessage = "密码的长度范围是6-20")]
        public string Password { get; set; }

        [Required]
        [StringLength(20,MinimumLength =6, ErrorMessage = "确认密码的长度范围是6-20")]
        [Compare("Password",ErrorMessage ="密码不一致")]
        public string PasswordAgain { get; set; }
    }
}
