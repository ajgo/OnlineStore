﻿using System.Collections.Generic;
using OnlineStore.Models;

namespace OnlineStore.Web.Areas.Admin.Models
{
    public class OrderSendViewModel
    {
        public Order Order { get; set; }
        public IEnumerable<ContactInfo> ContactInfos { get; set; }
    }
}