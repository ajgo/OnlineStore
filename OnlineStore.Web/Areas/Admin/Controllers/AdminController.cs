﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Web.Areas.Admin.Models;
using OnlineStore.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using OnlineStore.Services.Support;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IAccountService _accountService;

        public AdminController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            var loginVm = new LoginViewModel();
            return View(loginVm);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var admin = new OnlineStore.Models.Admin
            {
                Email = model.Email,
                Password = model.Password
            };

            try
            {
                var claimPrincipal = await _accountService.AdminLoginAsync(admin);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimPrincipal);
            }
            catch (EmailNotFoundException)
            {
                ModelState.AddModelError("", "邮箱不存在！");
                return View(model);
            }
            catch (PasswordErrorException)
            {
                ModelState.AddModelError("", "密码不合法！");
            }

            return RedirectToAction(nameof(Index), "Default");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (!model.Password.Equals(model.PasswordAgain))
            {
                //弹出注册失败对话框
                return Content("<script>alert('注册失败');history.go(-1);</script>");
            }

            var admin = new OnlineStore.Models.Admin
            {
                UserName = model.UserName,
                Password = model.Password
            };
            try
            {
                await _accountService.AdminRegisterAsync(admin);
            }
            catch (EmailExistException)
            {
                ModelState.AddModelError("", "邮箱已存在！");
                return View(model);
            }

            return RedirectToAction(nameof(Index));
        }

        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}