﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Models;
using OnlineStore.Services;

namespace OnlineStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ContactController : Controller
    {
        private readonly IAccountService _accountService;

        public ContactController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        // GET
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View(new ContactInfo());
        }

        [HttpPost]
        public IActionResult Create(ContactInfo info)
        {
            var adminId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value);

            _accountService.AdminAddContact(1, info);
            return RedirectToAction(nameof(Index));
        }
    }
}