﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnlineStore.Web.Models.User
{
    public class LoginViewModel
    {
        [DisplayName("账号")]
        [DataType(DataType.EmailAddress, ErrorMessage = "请输入Email")]
        [Required(ErrorMessage = "请输入邮箱")]
        public string Emails { get; set; }

        [DisplayName("密码")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "请输入密码")]
        public string Password { get; set; }
    }
}