﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnlineStore.Web.Models.User
{
    public class EditViewModel
    {

        [Key]
        public int id { get; set; }

        [DisplayName("用户名")]
        [Required(ErrorMessage = "请输入用户名")]
        [MaxLength(10, ErrorMessage = "用户名不能超过十个字符")]
        public string Name { get; set; }


        [DisplayName("邮箱")]
        [Required(ErrorMessage = "请输入Email地址")]
        [Description("以邮箱作为登录的账号")]
        [MaxLength(18, ErrorMessage = "Email的长度不能超过250个字符")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("电话号码")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNum { get;set; }
    }
}