﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.User
{
    public class AddAddressModel
    {
        [DisplayName("收货地址")]
        [Required(ErrorMessage ="地址不能为空")]
        public string MainAddress { set; get; }

        [DisplayName("收货人")]

        [Required(ErrorMessage = "收货人姓名不能为空")]
        public string UserName { set; get; }

        [DisplayName("联系电话")]

        [Required(ErrorMessage = "联系电话不能为空")]
        public string UserTelephone { set; get; }


    }
}
