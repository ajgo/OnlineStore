﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnlineStore.Web.Models.User
{
    public class RegisterViewModel
    {
 
            [Key]
            public int id { get; set; }

            [DisplayName("用户名")]
            [Required(ErrorMessage = "请输入用户名")]
            [MaxLength(10, ErrorMessage = "用户名不能超过十个字符")]
            public string Name { get; set; }


            [DisplayName("邮箱")]
            [Required(ErrorMessage = "请输入Email地址")]
            [Description("以邮箱作为登录的账号")]
            [MaxLength(18, ErrorMessage = "Email的长度不能超过250个字符")]
            [DataType(DataType.EmailAddress)]
            public string Email { get; set; }

            [DisplayName("密码")]
            [Required(ErrorMessage = "请输入密码")]
            [DataType(DataType.Password)]
            [MinLength(6, ErrorMessage = "密码需要6位以上的字符")]
            public string Password { get; set; }

            [Compare("Password", ErrorMessage = "两次输入的密码不一致，请重新输入")]
            [DisplayName("重复密码")]
            [Required(ErrorMessage = "请再次输入密码")]
            [DataType(DataType.Password)]
            [MinLength(6, ErrorMessage = "密码需要6位以上的字符")]
            public string RePassword { get; set; }

        
    }
}