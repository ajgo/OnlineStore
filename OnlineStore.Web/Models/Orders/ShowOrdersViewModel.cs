﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.Orders
{
    public class ShowOrdersViewModel
    {
        /// <summary>
        /// 获取所有订单信息
        /// </summary>
        /// 
        public List<Order> AllOrdersInfo { get; set; }
    }
}
