﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.User
{
    public class CommentModelView
    {


        [Required(ErrorMessage = "需要评分哦")]
        public int Score{get;set;}

        [Required(ErrorMessage = "要写评论哦")]
        [MaxLength(50, ErrorMessage = "评论不能超过50字")]
        public string commentString { get; set; }

        public int  productId { get; set; }

    }
}
