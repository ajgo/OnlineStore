﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.ProductDetails
{
    public class ProductDetailsModel
    {
        //主书籍
        public Product product { get; set; }

        //三本热卖书籍
        public IEnumerable<Product> products { get; set; }
    }
}
