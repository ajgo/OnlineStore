﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.ProductDetails
{
    public class AddOrderModel
    {

        //用户的多个收货信息
        public IEnumerable<ContactInfo> contactInfos { get; set; }

        //用户购物车信息
        public  IEnumerable<ProductInCart> productInCart { get; set; }

        //装载所有
    }
}
