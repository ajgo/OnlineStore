﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Web.Models.Home
{
    public class IndexViewModel
    {
        /// <summary>
        /// string 分类名称
        /// </summary>
        /// 
        //用户的信息
        public Dictionary<string, IEnumerable<Product>> CategoryBook { get; set; }
    }
}