﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OnlineStore.Models;

namespace OnlineStore.Services
{
    public interface IProductService
    {
        public Dictionary<string, IEnumerable<Product>> GetProductByType();
        public Task AddProductAsync(Product product, IEnumerable<IFormFile> images);
        public Task UpdateProductAsync(Product product);

        public Task AddToCart(int productId, int userId);

        public Task AddComment(int productId, int userId, ProductComment comment);

        public Product LoadProductWithDetails(int id);

        public IEnumerable<Product> GetAllProduct();

        public Task DeleteProduct(int id);

        public IEnumerable<Product> GetHotProduct(string type);

        public Product FindProductById(int id);

        public Dictionary<string, IEnumerable<Product>> FindProductByName(string name);

        public Dictionary<string, IEnumerable<Product>> FindProductByAuthor(string author);

        public Dictionary<string, IEnumerable<Product>> FindProductByPublisher(string publisher);

        public Dictionary<string, IEnumerable<Product>> FindProductByIsbn(string isbn);
    }
}