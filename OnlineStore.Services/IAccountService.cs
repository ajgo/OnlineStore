﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using OnlineStore.Models;

namespace OnlineStore.Services
{
    public interface IAccountService
    {
        // 管理员登入
        public Task<ClaimsPrincipal> AdminLoginAsync(Admin admin);

        // 管理员注册
        public Task AdminRegisterAsync(Admin admin);

        //管理员退出登录

        // 用户登入
        public Task<ClaimsPrincipal> UserLoginAsync(User user);

        // 用户注册
        public Task UserRegister(User user);
        
        public Task UserChangeInfo(User user);

        public Task AdminChangeInfo(Admin admin);

        public Task UserAddContact(int userId, ContactInfo contact);

        public IEnumerable<ContactInfo> GetUserContactInfo(int userId);
        public IEnumerable<ContactInfo> GetAdminContactInfo(int adminId);

        public Task AdminAddContact(int adminId, ContactInfo contact);
        
        //根据管理员邮箱查询该条信息 FindAdminByEmail
        public Admin FindAdminByEmail(string email);

        public IEnumerable<User> FindAllUser();
    }
}