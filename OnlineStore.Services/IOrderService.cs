﻿using System;
using OnlineStore.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineStore.Services
{
    public interface IOrderService
    {
        /// <summary>
        /// 查询所有订单
        /// </summary>
        /// <returns></returns>
        public List<Order> FindAllOrders();

        /// <summary>
        /// 查询带有带有状态的订单
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public List<Order> FindOrderWithState(OrderState state);

        /// <summary>
        /// 查询商品评论信息
        /// </summary>
        /// <returns></returns>
        public List<ProductComment> FindAllProductComment();

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="contactId"></param>
        public void SendOrder(Guid uid, int contactId);
        
        public Order FindOrderByUid(Guid uid);

        public Task SubmitOrder(int userId, int contactId, Dictionary<Product, int> products);
    }
}