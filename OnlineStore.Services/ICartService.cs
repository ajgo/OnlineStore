﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineStore.Models;

namespace OnlineStore.Services
{
    public interface ICartService
    {
        public IEnumerable<ProductInCart> GetProductInCart(int userId);

        public Task UpdateQuantityAsync(int productId, int quantity);

        public Task RemoveProduct(int productId);
    }
    
}