﻿using System;

namespace OnlineStore.Services.Support
{
    public class EmailExistException : ApplicationException
    {
        public EmailExistException()
        {
        }
    }

    public class EmailNotFoundException : ApplicationException
    {
        public EmailNotFoundException()
        {
        }
    }
    
    public class PasswordErrorException : ApplicationException
    {
        public PasswordErrorException()
        {
        }
    }

}