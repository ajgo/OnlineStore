﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    /// <summary>
    /// OrderDetail中的ContactInfo存储商家的联系方式
    /// </summary>
    public class OrderDetail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        // 外键
        public int? OrderId { get; set; }
        public int ContactInfoId { get; set; }
        
        // 导航属性
        public Order Order { get; set; }
        public ContactInfo ContactInfo { get; set; }
    }
}