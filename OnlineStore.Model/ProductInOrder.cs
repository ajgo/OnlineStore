﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class ProductInOrder
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Quantity { get; set; }
            
        // 外键
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        
        // 导航属性
        public Product Product { get; set; }
        public Order Order { get; set; }
    }
}