﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class Product
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }


        public string Isbn { get; set; }

        /// <summary>
        ///     用于展示的图片URL
        /// </summary>
        public string ImageUrl { get; set; }

        public string Description { get; set; }

        /// <summary>
        ///     商品剩余的数量（库存）
        /// </summary>
        public int Remain { get; set; }

        /// <summary>
        ///     销量
        /// </summary>
        public int SalesVolume { get; set; }

        /// <summary>
        ///     商品上架时间
        /// </summary>
        public DateTime ShelfTime { get; set; }

        public decimal Price { get; set; }


        // 导航属性
        public List<ProductComment> Comments { get; set; }
        public List<ProductInCart> ProductInCarts { get; set; }
        public List<ProductInOrder> ProductInOrders { get; set; }
        public List<ProductImage> ProductImages { get; set; }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}