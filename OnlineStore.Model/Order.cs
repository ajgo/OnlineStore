﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class Order
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        ///     唯一的订单号。
        ///     在表中，订单号相同的代表是同一笔订单
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        ///     订单产生的时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        ///     订单完成的时间
        /// </summary>
        public DateTime DoneTime { get; set; }

        public OrderState State { get; set; }

        // 外键
        public int UserId { get; set; }
        public int ContactInfoId { get; set; }

        // 导航属性
        public User User { get; set; }
        public OrderDetail OrderDetail { get; set; }
        public ContactInfo ContactInfo { get; set; }

        public List<ProductInOrder> ProductInOrders { get; set; }
    }

    public enum OrderState
    {
        // 订单等待商家啊处理
        WaitToDeal,

        // 订单已发货
        Shipped,

        // 订单已完成
        Completed
    }
}