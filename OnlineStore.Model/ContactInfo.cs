﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    /// <summary>
    ///     历史使用过的填写订单所用的信息
    /// </summary>
    public class ContactInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        // 外键
        public int? UserId { get; set; }
        public int? AdminId { get; set; }
        
        // 导航属性
        public User User { get; set; }
        public Admin Admin { get; set; }

    }
}