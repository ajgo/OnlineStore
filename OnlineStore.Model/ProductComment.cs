﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class ProductComment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Score { get; set; }
        public string Description { get; set; }
        public DateTime SubmitDate { get; set; }
        public bool IsAnonymous { get; set; }

        // 外键
        public int ProductId { get; set; }
        public int UserId { get; set; }
        
        // 导航属性
        public User User { get; set; }
        public Product Product { get; set; }
    }
}