﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class ProductInCart
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// 商品加入购物车的时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        
        /// <summary>
        /// 商品在购物车中的数量
        /// </summary>
        public int Quantity { get; set; }
        // 外键
        public int CartId { get; set; }
        public int ProductId { get; set; }
        
        // 导航属性
        public Product Product { get; set; }
        public Cart Cart { get; set; }
    }
}