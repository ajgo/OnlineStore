﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineStore.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }


        public string Password { get; set; }

        public string Email { get; set; }
        public string? Phone { get; set; }
        public RoleType Role { get; set; } = RoleType.General;
        public int Rank { get; set; } = 0;

        // 导航属性
        public Cart Cart { get; set; }
        public List<Order> Orders { get; set; }
    }

    public enum RoleType
    {
        General,
        Vip
    }
}