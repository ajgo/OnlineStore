﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineStore.Models;

namespace OnlineStore.Data
{
    public static class SeedData
    {
        private static readonly Random Random = new Random();
        private static int GenerateNext => Random.Next(50);
        private static int GenerateNextUtil5 => Random.Next(1, 5);

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new OnlineStoreDbContext(serviceProvider
                .GetRequiredService<DbContextOptions<OnlineStoreDbContext>>()))
            {
                context.Database.EnsureCreated();

                if (context.Products.Any())
                {
                    return;
                }

                var aJin = new Admin
                {
                    Email = "123aa123bb@qq.com",
                    Password = "123aa123bb",
                    UserName = "AJin"
                };

                context.Admins.Add(aJin);

                var ymcCart = new Cart();

                context.Carts.Add(ymcCart);

                var ymc = new User
                {
                    Name = "YMC",
                    Email = "123aa123bb@qq.com",
                    Password = "123aa123bb",
                    Phone = "10086",
                    Cart = ymcCart,
                    Rank = GenerateNext,
                    Role = RoleType.General
                };

                context.Users.Add(ymc);


                var ymcContact = new ContactInfo
                {
                    User = ymc,
                    Address = "电子科技大学五星级酒店",
                    Name = "YMC",
                    Phone = "1008611",
                };

                var aJinContact = new ContactInfo
                {
                    Admin = aJin,
                    Address = "电子科技大学茅草屋",
                    Name = "AJin",
                    Phone = "10010"
                };

                context.ContactInfos.AddRange(ymcContact, aJinContact);

                var ymcOrderCompletedDetail = new OrderDetail
                {
                    ContactInfo = aJinContact
                };
                var ymcOrderShippedDetail = new OrderDetail
                {
                    ContactInfo = aJinContact
                };
                var ymcOrderWaitToDealDetail = new OrderDetail
                {
                    ContactInfo = aJinContact
                };

                var ymcOrderCompleted = new Order
                {
                    State = OrderState.Completed,
                    Uid = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    DoneTime = DateTime.Now.AddDays(GenerateNextUtil5),
                    User = ymc,
                    ContactInfo = ymcContact,
                    OrderDetail = ymcOrderCompletedDetail
                };

                var ymcOrderShipped = new Order
                {
                    State = OrderState.Shipped,
                    Uid = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    User = ymc,
                    ContactInfo = ymcContact,
                    OrderDetail = ymcOrderShippedDetail
                };

                var ymcOrderWaitToDeal = new Order
                {
                    State = OrderState.WaitToDeal,
                    Uid = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    User = ymc,
                    ContactInfo = ymcContact,
                };


                context.OrderDetails.AddRange(ymcOrderCompletedDetail, ymcOrderShippedDetail, ymcOrderWaitToDealDetail);
                context.Orders.AddRange(ymcOrderCompleted, ymcOrderShipped, ymcOrderWaitToDeal);


                var comments = new List<ProductComment>
            {
                new ProductComment
                {
                    User = ymc,
                    IsAnonymous = true,
                    Score = GenerateNextUtil5,
                    SubmitDate = DateTime.Now,
                    Description = "YMC 大师写的一本好书",
                },
                new ProductComment
                {
                    User = ymc,
                    IsAnonymous = true,
                    Score = GenerateNextUtil5,
                    SubmitDate = DateTime.Now,
                    Description = "YMC 大师写的一本好书"
                },
                new ProductComment
                {
                    User = ymc,
                    IsAnonymous = true,
                    Score = GenerateNextUtil5,
                    SubmitDate = DateTime.Now,
                    Description = "YMC 大师写的一本好书"
                },
            };

                context.ProductComments.AddRange(comments);

                var images = new List<ProductImage>
            {
                new ProductImage
                {
                    Url = "/Image/Book1.png"
                },
                new ProductImage
                {
                    Url = "/Image/Book2.png"
                },
                new ProductImage
                {
                    Url = "/Image/Book1.png"
                },
                new ProductImage
                {
                    Url = "/Image/Book2.png"
                }
            };

                context.ProductImages.AddRange(images);

                var product1 = new Product
                {
                    Price = 50M,
                    Author = "YMC",
                    Type = "文学类",
                    Publisher = "YMC 出版社",
                    Description = "一本好书",
                    Isbn = Guid.NewGuid().ToString(),
                    Name = $"YMC Number{GenerateNext}",
                    Remain = GenerateNext,
                    SalesVolume = GenerateNext,
                    ShelfTime = DateTime.Now,
                    ImageUrl = "/Images/Book1.png",
                    Comments = comments,
                    ProductImages = images
                };


                var product2 = new Product
                {
                    Price = 50M,
                    Author = "YMC",
                    Type = "文学类",
                    Publisher = "YMC 出版社",
                    Description = "一本好书",
                    Isbn = Guid.NewGuid().ToString(),
                    Name = $"YMC Number{GenerateNext}",
                    Remain = GenerateNext,
                    SalesVolume = GenerateNext,
                    ShelfTime = DateTime.Now,
                    ImageUrl = "/Images/Book2.png",
                    Comments = comments,
                    ProductImages = images
                };
                var product3 = new Product
                {
                    Price = 50M,
                    Author = "YMC",
                    Type = "科技类",
                    Publisher = "YMC 出版社",
                    Description = "一本好书",
                    Isbn = Guid.NewGuid().ToString(),
                    Name = $"YMC Number{GenerateNext}",
                    Remain = GenerateNext,
                    SalesVolume = GenerateNext,
                    ShelfTime = DateTime.Now,
                    ImageUrl = "/Images/Book1.png",
                    Comments = comments,
                    ProductImages = images
                };

                var product4InCart = new List<ProductInCart>
            {
                new ProductInCart
                {
                    CreateTime = DateTime.Now,
                    Quantity = GenerateNextUtil5,
                    Cart = ymcCart
                }
            };
                context.ProductInCarts.AddRange(product4InCart);

                var product4InOrder = new List<ProductInOrder>
            {
                new ProductInOrder
                {
                    Order = ymcOrderCompleted,
                    Quantity = GenerateNextUtil5
                },
                new ProductInOrder
                {
                    Order = ymcOrderShipped,
                    Quantity = GenerateNextUtil5
                },
                new ProductInOrder
                {
                    Order = ymcOrderWaitToDeal,
                    Quantity = GenerateNextUtil5
                }
            };

                context.ProductInOrders.AddRange(product4InOrder);

                var product4 = new Product
                {
                    Price = 50M,
                    Author = "YMC",
                    Type = "科技类",
                    Publisher = "YMC 出版社",
                    Description = "一本好书",
                    Isbn = Guid.NewGuid().ToString(),
                    Name = $"YMC Number{GenerateNext}",
                    Remain = GenerateNext,
                    SalesVolume = GenerateNext,
                    ShelfTime = DateTime.Now,
                    ImageUrl = "/Images/Book2.png",
                    Comments = comments,
                    ProductImages = images,
                    ProductInCarts = product4InCart,
                    ProductInOrders = product4InOrder
                };

                context.AddRange(product1, product2, product3, product4);
                context.SaveChanges();
            };


        }
    }
}